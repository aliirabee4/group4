//import 'package:flutter/material.dart';
//import 'package:g4_first_project/bottomNavigationScreens/msg.dart';
//import 'package:g4_first_project/bottomNavigationScreens/notifications.dart';
//import 'package:g4_first_project/bottomNavigationScreens/search.dart';
//
//import 'bottomNavigationScreens/home.dart';
//
//class BottomNavigationScreen extends StatefulWidget {
//  @override
//  _BottomNavigationScreenState createState() => _BottomNavigationScreenState();
//}
//
//class _BottomNavigationScreenState extends State<BottomNavigationScreen> {
//  int _currentIndex = 0;
//
//  List _screens = [
//    HomeScreen(),
//    SearchScreen(),
//    NotificationsScreen(),
//    MsgScreen()
//  ];
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      bottomNavigationBar: BottomNavigationBar(
//        currentIndex: _currentIndex,
//        type: BottomNavigationBarType.fixed,
//        onTap: (selecteItem) {
//          setState(() {
//            _currentIndex = selecteItem;
//            print(_currentIndex);
//          });
//        },
//        items: [
//          BottomNavigationBarItem(
//              icon: Icon(
//                Icons.home,
//                size: 30,
//              ),
//              title: Text('')),
//          BottomNavigationBarItem(
//              icon: Icon(
//                Icons.search,
//                size: 30,
//              ),
//              title: Text('')),
//          BottomNavigationBarItem(
//              icon: Icon(
//                Icons.notifications,
//                size: 30,
//              ),
//              title: Text('')),
//          BottomNavigationBarItem(
//              icon: Icon(
//                Icons.mail_outline,
//                size: 30,
//              ),
//              title: Text(''))
//        ],
//      ),
//      body: _screens[_currentIndex],
//    );
//  }
//}
