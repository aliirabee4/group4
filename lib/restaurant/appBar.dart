import 'package:flutter/material.dart';

Widget customAppBar() {
  return AppBar(
    elevation: 0,
    backgroundColor: Colors.white,
    centerTitle: true,
    title: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Punk',
          style: TextStyle(
              color: Colors.yellow, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        Text(
          'Food',
          style: TextStyle(
              color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 18),
        ),
      ],
    ),
    leading: IconButton(
        icon: Icon(
          Icons.menu,
          color: Colors.black,
        ),
        onPressed: () {}),
    actions: [
      Stack(
        children: [
          IconButton(
              icon: Icon(
                Icons.notifications,
                color: Colors.grey,
              ),
              onPressed: () {}),
          Positioned(
            top: 15,
            left: 23,
            child: Container(
              width: 10,
              height: 10,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8), color: Colors.red),
            ),
          )
        ],
      )
    ],
  );
}
