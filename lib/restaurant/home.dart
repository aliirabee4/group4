import 'package:flutter/material.dart';
import 'package:g4_first_project/restaurant/appBar.dart';
import 'package:g4_first_project/restaurant/order.dart';

class RestaurantHomeScreen extends StatefulWidget {
  @override
  _RestaurantHomeScreenState createState() => _RestaurantHomeScreenState();
}

class _RestaurantHomeScreenState extends State<RestaurantHomeScreen> {
  List<Map<String, dynamic>> _catList = [
    {'name': 'Cheese', 'isSelected': true},
    {'name': 'Burger', 'isSelected': false},
    {'name': 'Chicken', 'isSelected': false},
    {'name': 'Beef', 'isSelected': false},
    {'name': 'Snacks', 'isSelected': false},
  ];

  int selectedCat = 0;

  Widget _catCard({String catName, bool isSelected, int index}) {
    return InkWell(
      onTap: () {
        setState(() {
          _catList[selectedCat]['isSelected'] = false;
          selectedCat = index;
          _catList[selectedCat]['isSelected'] = true;
        });
      },
      child: Container(
        height: 60,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
        child: Column(
          children: [
            Text(
              catName,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              width: 25,
              height: 2,
              color: isSelected ? Colors.yellowAccent : Colors.white,
            )
          ],
        ),
      ),
    );
  }

  Widget _mealCard() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => OrderScreen()));
      },
      child: Container(
        width: MediaQuery.of(context).size.width / 2,
        height: 220,
        margin: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.grey.withOpacity(0.1)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(90),
                  color: Colors.white70,
                  image: DecorationImage(
                      image: NetworkImage(
                          'https://img.freepik.com/free-vector/hamburger-ingredients_98292-3567.jpg?size=338&ext=jpg&ga=GA1.2.585400526.1585353645'))),
            ),
            SizedBox(
              height: 15,
            ),
            Text('Double Cheese Burger'),
            SizedBox(
              height: 10,
            ),
            Text('MacDonalds'),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: customAppBar(),
      body: Column(
        children: [
          Directionality(
            textDirection: TextDirection.ltr,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: TextFormField(
                decoration: InputDecoration(
                    hintText: 'Search here',
                    prefixIcon: Icon(Icons.search),
                    contentPadding: EdgeInsets.all(10),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50))),
              ),
            ),
          ),
          SizedBox(
            height: 60,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              itemBuilder: (ctx, index) {
                return _catCard(
                    catName: _catList[index]['name'],
                    index: index,
                    isSelected: _catList[index]['isSelected']);
              },
            ),
          ),
          SizedBox(
            height: 220,
            child: ListView.builder(
                itemCount: 10,
                scrollDirection: Axis.horizontal,
                itemBuilder: (ctx, index) {
                  return _mealCard();
                }),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
            child: Row(
              children: [
                Text(
                  'Offers & Discounts',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                )
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 180,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
                image: DecorationImage(
                    image: NetworkImage(
                        'https://image.freepik.com/free-vector/commercial-banner-big-sale-lettering-with-thirty-fifty-percentage-discount_24877-57620.jpg'),
                    fit: BoxFit.cover)),
          )
        ],
      ),
    );
  }
}
