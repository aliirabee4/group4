import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedItem = 0;
  List<Map<String, dynamic>> _dataList = [
    {
      'title': 'Coffee',
      'isSelected': true,
      'image':
          'https://t3.ftcdn.net/jpg/02/06/38/82/240_F_206388213_ru7sAILyCmDtvCGPRT2bSKCyJKRH1e2j.jpg'
    },
    {
      'title': 'Tea',
      'isSelected': false,
      'image':
          'https://t3.ftcdn.net/jpg/02/06/38/82/240_F_206388213_ru7sAILyCmDtvCGPRT2bSKCyJKRH1e2j.jpg'
    },
    {
      'title': 'Juice',
      'isSelected': false,
      'image':
          'https://t3.ftcdn.net/jpg/02/06/38/82/240_F_206388213_ru7sAILyCmDtvCGPRT2bSKCyJKRH1e2j.jpg'
    }
  ];

  Widget _item({String title, String imageUrl, int index, bool selected}) {
    return InkWell(
      onTap: () {
        setState(() {
          _dataList[_selectedItem]['isSelected'] = false;
          _selectedItem = index;
          _dataList[_selectedItem]['isSelected'] = true;
        });
      },
      child: Container(
        width: 120,
        height: 50,
        margin: EdgeInsets.symmetric(
          horizontal: 5,
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40),
            color: selected ? Colors.yellow : Colors.grey),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(title),
            Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                  image: DecorationImage(image: NetworkImage(imageUrl)),
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.white),
            )
          ],
        ),
      ),
    );
  }

  Widget _promoCard() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 160,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.2),
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: Colors.grey, width: 1)),
      child: Row(
        children: [
          Container(
            width: MediaQuery.of(context).size.width / 2,
            height: 160,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        'https://img.freepik.com/free-vector/realistic-coffee-background-with-drawings_79603-603.jpg?size=626&ext=jpg&ga=GA1.2.585400526.1585353645'),
                    fit: BoxFit.cover)),
          ),
          SizedBox(
            width: 30,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                'Coffee Time',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text('9 am : 3:30 pm',
                  textDirection: TextDirection.ltr,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              Container(
                height: 40,
                width: 120,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.brown),
                child: Center(
                  child: Text(
                    ' 50 % Discount',
                    textDirection: TextDirection.ltr,
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _productCard() {
    return Container(
      width: 100,
      margin: EdgeInsets.symmetric(horizontal: 5),
      height: 130,
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.3),
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: Colors.grey, width: 1)),
      child: Column(
        children: [
          Container(
            width: 100,
            height: 80,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                      'https://t3.ftcdn.net/jpg/02/06/38/82/240_F_206388213_ru7sAILyCmDtvCGPRT2bSKCyJKRH1e2j.jpg',
                    ),
                    fit: BoxFit.cover)),
          ),
          Text('Espresso'),
          Text('50 LE')
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            'Good Morning Ahmed',
            style: TextStyle(color: Colors.black, fontSize: 15),
          ),
          actions: [
            IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Colors.black,
                ),
                onPressed: () {})
          ],
          leading: Container(
            width: 40,
            height: 40,
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        'https://t4.ftcdn.net/jpg/03/32/59/65/240_F_332596535_lAdLhf6KzbW6PWXBWeIFTovTii1drkbT.jpg')),
                borderRadius: BorderRadius.circular(40),
                color: Colors.red),
          ),
        ),
        body: Column(
          children: [
            Directionality(
              textDirection: TextDirection.ltr,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: TextFormField(
                  decoration: InputDecoration(
                      hintText: 'Search for coffee cup',
                      prefixIcon: Icon(Icons.search),
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50))),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              height: 50,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 3,
                itemBuilder: (ctx, index) {
                  return _item(
                      index: index,
                      title: _dataList[index]['title'],
                      selected: _dataList[index]['isSelected'],
                      imageUrl: _dataList[index]['image']);
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '<<',
                    style: TextStyle(fontSize: 20),
                  ),
                  Text('Available Promo', style: TextStyle(fontSize: 20)),
                ],
              ),
            ),
            _promoCard(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text('Yor Recommendation', style: TextStyle(fontSize: 20)),
                ],
              ),
            ),
            Container(
              height: 130,
              child: ListView.builder(
                  itemCount: 10,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (ctx, index) {
                    return _productCard();
                  }),
            )
          ],
        ),
      ),
    );
  }
}
