import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:g4_first_project/restaurant/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Ahmed Mousa',
      home: RestaurantHomeScreen(),
    );
  }
}

// class LoginScreen extends StatefulWidget {
//   @override
//   _LoginScreenState createState() => _LoginScreenState();
// }

// class _LoginScreenState extends State<LoginScreen> {
//   Widget _container() {
//     return Row(
//       children: <Widget>[
//         Container(
//           width: 100,
//           height: 40,
//           margin: EdgeInsets.only(bottom: 10),
//           padding: EdgeInsets.only(left: 10, top: 10),
//           decoration: BoxDecoration(
//               color: Colors.red, borderRadius: BorderRadius.circular(5)),
//           child: Text('data'),
//         ),
//       ],
//     );
//   }

//   Widget _item({String title}) {
//     return Container(
//       width: 150,
//       height: 160,
//       margin: EdgeInsets.only(top: 20),
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(5),
//         color: Colors.white,
//       ),
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: <Widget>[
//           Container(
//             width: 100,
//             height: 100,
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(90), color: Colors.green),
//           ),
//           Text(title),
//           Container(
//             width: 150,
//             height: 2,
//             color: Colors.red,
//           )
//         ],
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         backgroundColor: Colors.white70,
//         appBar: AppBar(
//           elevation: 0,
//           backgroundColor: Colors.white,
//           title: Text(
//             'حسابي',
//             style: TextStyle(
//                 color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
//           ),
//           centerTitle: true,
//           automaticallyImplyLeading: true,
//           actions: <Widget>[
//             Icon(
//               Icons.notifications,
//               color: Colors.red,
//               size: 30,
//             )
//           ],
//         ),
//         body: ListView(
//           children: <Widget>[
//             Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[_item(title: 'طلباتي'), _item(title: 'حسابي')],
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[
//                 _item(title: 'طلبات العملاء'),
//                 _item(title: 'انشاء عقد')
//               ],
//             )
//           ],
//         )
//         // ListView(scrollDirection: Axis.horizontal, children: <Widget>[
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         //   _container(),
//         // ]),
//         );
//   }
// }
