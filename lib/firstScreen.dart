// import 'package:flutter/material.dart';
// import 'package:g4_first_project/secenodScreen.dart';

// class FirstScreen extends StatefulWidget {
//   @override
//   _FirstScreenState createState() => _FirstScreenState();
// }

// class _FirstScreenState extends State<FirstScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           centerTitle: true,
//           title: Text('First Screen'),
//         ),
//         body: Center(
//           child: InkWell(
//             onTap: () {
//               Navigator.of(context).push(MaterialPageRoute(
//                 builder: (context) => SeconedScreen(),
//               ));
//             },
//             child: Container(
//               width: 200,
//               height: 40,
//               decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(10),
//                   color: Colors.purple),
//               child: Center(
//                 child: Text(
//                   'Go To Seconed Screen',
//                   style: TextStyle(color: Colors.white),
//                 ),
//               ),
//             ),
//           ),
//         ));
//   }
// }
